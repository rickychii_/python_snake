"""
Copyright 2023 Rickychii_
Code is open source, you can download and modify whatever you want.
"""

from pyray import *
import time
from Game import Game


init_window(Game.windowWidth, Game.windowHeight, "Camillus Softwares")
game = Game()

while not window_should_close():
    begin_drawing()
    game.draw()
    end_drawing()
    game.update()
    time.sleep(1 / game.fps)


close_window()