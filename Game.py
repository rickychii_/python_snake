from pyray import *
from copy import copy
import random
from Cella import Cella

class Game:
    
    windowWidth = 1024
    windowHeight = 768

    fps = 10

    snake = []
    length = 10

    starty = 40
    righex = 60
    righey = 60

    deltax = windowWidth / righex
    deltay = (windowHeight - starty) / righey

    upPressed = False
    downPressed = False
    rightPressed = False
    leftPressed = False

    direzione = ""
    
    def __init__(self):
        
        for x in range(0, self.length, 1):
            c = Cella(x,0)
            self.snake.insert(0,c)

        self.direzione = "right"

        self.apple = load_image("apple.png")
        self.textureapple = load_texture_from_image(self.apple)

        self.activeAppleX = 0
        self.activeAppleY = 0

        self.gameStatus = "gaming"

        self.eatedApple = 0
        self.livello = 1

        self.__generateApple()
        self.__printLevel()
        
    def __drawGrid(self):
        #DRAW X LINES
        for x in range(1, self.righex, 1):
            draw_line(int(x * self.deltax), self.starty, int(x * self.deltax), self.windowHeight, BLACK)
        #DRAW Y LINES
        for y in range(1, self.righey, 1):
            draw_line(0, int(y * self.deltay) + self.starty, self.windowWidth, int(y * self.deltay) + self.starty, BLACK)

    def __drawScore(self):
        draw_line(0,self.starty,self.windowWidth,self.starty,BLACK)

    def __drawSnake(self):
        for x in range(0, self.length , 1):
            draw_rectangle(int(self.snake[x].x * self.deltax),int(self.snake[x].y * self.deltay) + self.starty,int(self.deltax + 1),int(self.deltay + 1),BLUE)
            if(x == 0):
                draw_ellipse(
                    int(self.snake[x].x * self.deltax) + 4, 
                    int(self.snake[x].y * self.deltay) + self.starty + 4,
                    5,5,RED)
    
    def __allungaSnake(self):
        self.snake.append(
            Cella
            (
                self.snake[self.length-1].x, 
                self.snake[self.length-1].y)
            )
        self.length += 1


    def __checkAppleEated(self):
        if (self.snake[0].x == self.activeAppleX 
            and self.snake[0].y == self.activeAppleY
            ):
            
            self.__allungaSnake()
            self.__generateApple()
            
            self.eatedApple += 1
            print("apple ate: " + str(self.eatedApple))

            if(self.eatedApple % 10 == 0):
                self.fps +=3
                self.livello += 1
                self.__printLevel()

    def __checkSnakeCollision(self):
        for x in range(1, self.length , 1):
            if(self.snake[0].x == self.snake[x].x and
                self.snake[0].y == self.snake[x].y):
                self.gameStatus = "gameOver"
                break

    def __generateApple(self):
        insideSnake = True
        while(insideSnake == True):
            self.activeAppleX = random.randint(0, self.righex - 1)
            self.activeAppleY = random.randint(0, self.righey - 1)
            for x in range(0, self.length , 1):
                if (
                    self.snake[x].x == self.activeAppleX 
                    and
                    self.snake[x].y == self.activeAppleY
                ):
                    insideSnake = True
                    break
                insideSnake = False

    def __printLevel(self):
        print("livello " + str(self.livello))


    ##MAINDRAW
    def draw(self):
        if(self.gameStatus == "gaming"):
            clear_background([0xcc,0xaa,0xff])
            
            #draw apple
            draw_texture_ex(self.textureapple, Vector2(int(self.activeAppleX * self.deltax), int(self.activeAppleY * self.deltay) + self.starty), 0.0, 0.16, WHITE)

            #draw score
            draw_rectangle(0,0,self.windowWidth,int(self.starty),WHITE)
            draw_text("Level: " + str(self.livello),10,10,20,RED)
            draw_text("Apple Ate: " + str(self.eatedApple),self.windowWidth - 180,10,20,RED)

            self.__drawScore()
            self.__drawSnake()
            self.__drawGrid()
        elif(self.gameStatus == "gameOver"):
            clear_background([0x22,0x22,0x44])
            draw_text("Game Over", 10, 10, 40, RED)
            draw_text("Level: " + str(self.livello),10,80,20,RED)
            draw_text("Apple Ate: " + str(self.eatedApple),10,110,20,RED)

    ##MAINUPDATE
    def update(self):

        if(self.gameStatus == "gaming"):
        
            #let's move the snake
            oldsnake0 = copy(self.snake[0])

            if(self.direzione == "right"):
                self.snake[0].x += 1
            if(self.direzione == "left"):
                self.snake[0].x -= 1
            if(self.direzione == "up"):
                self.snake[0].y -= 1
            if(self.direzione == "down"):
                self.snake[0].y += 1

            for x in range(self.length - 1, 1, -1):
                self.snake[x] = self.snake[x - 1]
        
            self.snake[1] = oldsnake0
            
            #guards:
            if(self.snake[0].x >= self.righex): self.snake[0].x = 0
            if(self.snake[0].x < 0): self.snake[0].x = self.righex - 1
            if(self.snake[0].y >= self.righey): self.snake[0].y = 0
            if(self.snake[0].y < 0): self.snake[0].y = self.righey - 1

            #key listener
            self.upPressed = is_key_down(KEY_UP)
            self.downPressed = is_key_down(KEY_DOWN)
            self.rightPressed = is_key_down(KEY_RIGHT)
            self.leftPressed = is_key_down(KEY_LEFT)
            if(self.upPressed and (self.direzione == "left" or self.direzione == "right")):
                self.direzione = "up"
            if(self.downPressed and (self.direzione == "left" or self.direzione == "right")):
                self.direzione = "down"
            if(self.leftPressed and (self.direzione == "up" or self.direzione == "down")):
                self.direzione = "left"
            if(self.rightPressed and (self.direzione == "up" or self.direzione == "down")):
                self.direzione = "right"

            #DEBUG
            #if(is_key_pressed(KEY_LEFT_CONTROL)):
            #    self.__allungaSnake()

            #if(is_key_pressed(KEY_RIGHT_CONTROL)):
            #    self.__generateApple()

            self.__checkAppleEated()

            self.__checkSnakeCollision()